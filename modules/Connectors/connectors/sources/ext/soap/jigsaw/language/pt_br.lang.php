<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $connector_strings = array ( 
'LBL_STOCK_EXCHANGE'=>'Bolsa de Valores',
'jigsaw_api_key'=>'Chave API',
'LBL_CITY'=>'Cidade',
'LBL_SIC_CODE'=>'Código CAE',
'LBL_STOCK_SYMBOL'=>'Código na Bolsa de Valores',
'LBL_CREATED_ON'=>'Data da Criação do Perfil',
'ERROR_API_CALL'=>'ERRO:',
'LBL_STREET'=>'Endereço',
'LBL_REVENUE_RANGE'=>'Estimativa de receita anual',
'LBL_EMPLOYEE_RANGE'=>'Headcount Range',
'LBL_ID'=>'ID da Empresa',
'LBL_INDUSTRY1'=>'Industria Primária',
'LBL_LICENSING_INFO'=>'Informação de Licenciamento',
'LBL_COMPANY_NAME'=>'Nome de Empresa',
'LBL_EMPLOYEE_COUNT'=>'Nº Colaboradores',
'range_end'=>'Número máximo de resultados para listagem',
'LBL_COUNTRY'=>'País',
'LBL_OWNERSHIP'=>'Propriedade',
'LBL_REVENUE'=>'Receita Anual',
'LBL_WEBSITE'=>'Site Internet',
'LBL_LINKED_IN_JIGSAW'=>'Site Internet Jigsaw',
'LBL_PHONE'=>'Telefone',
'LBL_STATE'=>'UF',
'jigsaw_wsdl'=>'URL do WSDL',
);
?>
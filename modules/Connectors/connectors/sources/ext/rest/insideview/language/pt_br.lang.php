<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $connector_strings = array ( 
'LBL_ENGAGE'=>'Captar Clientes',
'iv_description0'=>'Clicando em "Começar" você está de acordo com InsideView&#039;s',
'LBL_GET_STARTED'=>'Começar!',
'LBL_OPP'=>'Encontrar Oportunidades',
'LBL_TOS0'=>'Estou de acordo com InsideView&#039;s',
'LBL_REFERRAL'=>'Obter Referências',
'LBL_TOS3'=>'Política de Privacidade',
'LBL_TOS1'=>'Termos de Serviço',
'LBL_ENGAGE_SUB'=>'com iniciadores de conversão',
'LBL_OPP_SUB'=>'para alcançar os clientes',
'LBL_REFERRAL_SUB'=>'para decisões chave',
);
?>
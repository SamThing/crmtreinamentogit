<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $connector_strings = array ( 
'LBL_ZIP'=>'CEP',
'api_key'=>'Chave API',
'LBL_CITY'=>'Cidade',
'partner_code'=>'Código API de parceiro',
'LBL_COMPANY_TICKER'=>'Código na Bolsa de Valores',
'LBL_DESCRIPTION'=>'Descrição',
'ERROR_LBL_CONNECTION_PROBLEM'=>'Erro: Impossível ligar ao servidor para obter informação Zoominfo.',
'LBL_EMPLOYEES'=>'Funcionários',
'LBL_COMPANY_ID'=>'ID',
'LBL_INDUSTRY'=>'Indústria',
'LBL_COMPANY_NAME'=>'Nome de Empresa',
'LBL_COUNTRY'=>'País',
'LBL_REVENUE'=>'Receita Anual',
'LBL_STREET'=>'Rua',
'LBL_WEBSITE'=>'Site Internet',
'LBL_SEARCH_FIELDS_INFO'=>'São suportados pelo © Zoominfo Company os seguintes campos; API: Nome da Entidade, Cidade, UF e País.',
'LBL_PHONE'=>'Telefone',
'LBL_STATE'=>'UF',
'company_detail_url'=>'URL de detalhe de empresa',
'LBL_ZOOMINFO_COMPANY_URL'=>'URL de perfil de empresa',
'company_search_url'=>'URL de pesquisa de empresa',
'LBL_LICENSING_INFO'=>'ZoomInfo © fornece informações sobre mais de 45 milhões de pessoas de negócios em mais de 5 milhões de empresas. Saiba mais em http://www.zoominfo.com/about',
);
?>
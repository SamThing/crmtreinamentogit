<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'BG_HEADER'=>'#c7d4d2',
'BG_SUB_HEADER'=>'#dee7ed ',
'LBL_FILE_SUFFIX'=>'.lang.php',
'LBL_LANG_PACK_FILE_SUFFIX'=>'.zip ',
'LBL_SEPARATOR'=>':',
'LBL_ARCHIVES'=>'Arquivos',
'LBL_AUTO_UPDATE_COMMENT'=>'Atualizar automaticamente sugestões, se mostradas',
'LBL_AUTO_UPDATE'=>'Atualização Automática',
'LBL_ARCHIVES_AUTHOR'=>'Autor',
'LBL_LANG_PACK_AUTHOR'=>'Autor:',
'LBL_LOADING'=>'Carregando...',
'LBL_BUTTON_CREATE_LANG_PACK'=>'Criar',
'LBL_CREATE_LANG_PACK'=>'Criar Pacote de Idioma',
'LBL_LANG_PACK_DATE'=>'Data de Publicação:',
'LBL_SETTINGS'=>'Definições',
'LBL_ARCHIVES_DESCRIPTION'=>'Descrição',
'LBL_LANG_PACK_DESC'=>'Descrição:',
'LBL_DROPDOWN'=>'Dropdown :',
'LBL_EXAMPLE_LANG_PACK_VERSION'=>'Exemplo: 1.0 ',
'LBL_EXAMPLE_LANG_PACK_DATE'=>'Exemplo: 11-02-2008',
'LBL_EXAMPLE_LANG_PACK_AUTHOR'=>'Exemplo: John Doe',
'LBL_EXAMPLE_LANG_PACK_NAME'=>'Exemplo: Pacote de Idioma Sueco',
'LBL_EXAMPLE_LANG_PACK_DESC'=>'Exemplo: SugarCRM traduzido para Sueco',
'LBL_ARCHIVES_SHOW_HIDE'=>'Mostrar / Ocultar Arquivos',
'LBL_CREATE_LANG_PACK_SHOW_HIDE'=>'Mostrar / Ocultar Criar Pacote de Idioma',
'LBL_SETTTINGS_SHOW_HIDE'=>'Mostrar / Ocultar Definições',
'LBL_MODULE'=>'Módulo :',
'LBL_NONE_FOUND'=>'Nenhumas sugestões foram encontradas',
'LBL_ARCHIVES_NAME'=>'Nome',
'LBL_FILENAME_COMMENT'=>'Nome do arquivo para o qual exportar o pacote de idioma',
'LBL_FILE_NAME'=>'Nome do arquivo:',
'LBL_LANG_PACK_NAME'=>'Nome:',
'LBL_BY_LABEL'=>'Por Rótulo',
'LBL_BY_STRING'=>'Por String',
'LBL_NO_LANG_SELECTED'=>'Por favor, insira um idioma para traduzir o SugarCRM',
'LBL_PREFIX_COMMENT'=>'Prefixo arquivo idioma, ex: en_us, se, no ',
'LBL_PREFIX'=>'Prefixo:',
'LBL_ARCHIVES_PUBLISHED'=>'Publicado',
'LBL_LABEL'=>'Rótulo',
'LBL_LABEL_HEADER'=>'Rótulo :',
'LBL_BUTTON_SAVE'=>'Salvar',
'LBL_STD_VALUE'=>'Valor Padrão',
'LBL_TRANSLATED'=>'Valor Traduzido',
'LBL_ARCHIVES_VERSION'=>'Versão',
'LBL_LANG_PACK_VERSION'=>'Versão:',
);
?>
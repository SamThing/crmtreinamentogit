<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'Atividades',
'LBL_ASSIGNED_TO_NAME'=>'Atribuido a ',
'LBL_PARENT_CATEGORY'=>'Categoria Pai',
'LBL_PRODUCT_CATEGORYS_NAME'=>'Categoria Pai',
'LBL_PRODUCT_CATEGORIES'=>'Categoria Pai',
'LBL_AOS_PRODUCT_CATEGORIES_SUBPANEL_TITLE'=>'Categorias de Produto',
'LBL_MODULE_TITLE'=>'Categorias do Produto',
'LBL_MODULE_NAME'=>'Categorias do Produto',
'LBL_CREATED_USER'=>'Criado Por',
'LBL_CREATED_ID'=>'Criado Por',
'LBL_CREATED'=>'Criado Por',
'LNK_NEW_RECORD'=>'Criar Categoria do Produto',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DELETED'=>'Deletado',
'LBL_REMOVE'=>'Deletar',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_EDIT_BUTTON'=>'Editar',
'LBL_ID'=>'ID',
'LBL_ASSIGNED_TO_ID'=>'ID do Usuário Atribuido',
'LNK_IMPORT_AOS_PRODUCT_CATEGORIES'=>'Importar Categorias do Produto',
'LBL_LIST_FORM_TITLE'=>'Lista das Categorias de Produto',
'LBL_HOMEPAGE_TITLE'=>'Minhas Categorias de Produto',
'LBL_MODIFIED_NAME'=>'Modificado Por',
'LBL_MODIFIED'=>'Modificado Por',
'LBL_MODIFIED_ID'=>'Modificado Por',
'LBL_MODIFIED_USER'=>'Modificado Por',
'LBL_LIST_NAME'=>'Nome',
'LBL_NAME'=>'Nome',
'LBL_NEW_FORM_TITLE'=>'Nova Categoria do Produto',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar por Categoria do Produto',
'LBL_SUB_CATEGORIES'=>'Subcategorias',
'LNK_LIST'=>'Visualizar Categorias do Produto',
'LBL_HISTORY_SUBPANEL_TITLE'=>'Visualizar Histórico',
'LBL_IS_PARENT'=>'É categoria pai',
);
?>
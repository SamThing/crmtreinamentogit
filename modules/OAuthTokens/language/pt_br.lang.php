<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_OAUTH_CONSUMERREQ'=>'Autorizar Toke do consumidor (s)',
'LBL_OAUTH_AUTHORIZE'=>'Autorizar Token',
'LBL_OAUTH_VALIDATION'=>'CEodigo de verificação',
'LBL_TS'=>'Data e Hora',
'LBL_LIST_DELETE'=>'Deletar Token',
'LBL_ID'=>'ID',
'LBL_CONSUMER'=>'Nome do Consumidor',
'LBL_OAUTH_ROLE'=>'Perfil Token',
'LBL_OAUTH_REQUEST'=>'Requerer Token',
'LBL_STATUS'=>'Status',
'LBL_OAUTH_DISABLED'=>'Suporte OAuth não habilitado. Extensão PHP oauth pode estar faltando. Por favor, contate o administrador.',
'LBL_ASSIGNED_TO_NAME'=>'Usuário',
);
?>
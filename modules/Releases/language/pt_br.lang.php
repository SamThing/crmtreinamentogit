<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'NTC_STATUS'=>'Altere o status para Inativo para remover esta Versão da lista dropdown do campo Versão.',
'NTC_LIST_ORDER'=>'Configure a ordem pela qual a versão irá aparecer nas listas suspensas',
'LBL_EDITLAYOUT'=>'Editar Layout',
'LBL_LIST_FORM_TITLE'=>'Lista de Versões',
'LNK_NEW_RELEASE'=>'Lista de Versões',
'LBL_NEW_FORM_TITLE'=>'Nova Versão',
'LBL_LIST_LIST_ORDER'=>'Ordem',
'LBL_LIST_ORDER'=>'Ordem:',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar Versões',
'LBL_LIST_STATUS'=>'Status',
'LBL_STATUS'=>'Status:',
'NTC_DELETE_CONFIRMATION'=>'Tem a certeza que deseja excluir este registo?',
'ERR_DELETE_RECORD'=>'Um número de registro deve ser especificado para excluir esta versão.',
'LBL_LIST_NAME'=>'Versão',
'LBL_NAME'=>'Versão:',
'LBL_RELEASE'=>'Versão:',
'LBL_MODULE_NAME'=>'Versões',
'LBL_MODULE_TITLE'=>'Versões: Tela Principal',
 'release_status_dom' => array ( 
''=>'',
'Active'=>'Ativo',
'Inactive'=>'Inativo',
),
);
?>
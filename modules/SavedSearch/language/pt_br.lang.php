<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_LIST_FORM_TITLE'=>'A Minha Lista de Pesquisas Salvas',
'LBL_DELETE_BUTTON_TITLE'=>'Apagar esta Pesquisa Salva',
'LBL_MODULE_TITLE'=>'As Minhas Pesquisas Salvas',
'LBL_SEARCH_FORM_TITLE'=>'As Minhas Pesquisas Salvas: Pesquisar',
'LBL_ASCENDING'=>'Ascendente',
'LBL_UPDATE_BUTTON_TITLE'=>'Atualizar esta Pesquisa Salva',
'LBL_DESCENDING'=>'Descendente',
'LBL_DIRECTION'=>'Direção:',
'LBL_PREVIOUS_SAVED_SEARCH_HELP'=>'Editar e Excluir uma Pesquisa Salva.',
'LBL_SAVE_SEARCH_AS_HELP'=>'Esta operação salva as suas configurações de visualização e qualquer filtro da Pesquisa Avançada.',
'LBL_MODIFY_CURRENT_SEARCH'=>'Modificar pesquisa atual',
'LBL_LIST_MODULE'=>'Módulo',
'LBL_LIST_NAME'=>'Nome',
'LBL_ORDER_BY_COLUMNS'=>'Ordenar por coluna:',
'LBL_PREVIOUS_SAVED_SEARCH'=>'Pesquisas anteriormente salvas:',
'LBL_SAVE_BUTTON_TITLE'=>'Salvar a Pesquisa atual',
'LBL_SAVE_SEARCH_AS'=>'Salvar esta Pesquisa como:',
'LBL_DELETE_CONFIRM'=>'Tem a certeza que deseja excluir as pesquisas salvas selecionadas?',
);
?>
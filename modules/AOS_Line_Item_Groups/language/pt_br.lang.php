<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DELETED'=>'Deletado',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_DISCOUNT_AMOUNT'=>'Disconto',
'LBL_MODULE_NAME'=>'Grupos',
'LBL_ID'=>'ID',
'LBL_TAX_AMOUNT'=>'Impostos',
'LBL_NAME'=>'Nome do Grupo',
'LBL_LIST_NUM'=>'Número',
'LBL_SUBTOTAL_AMOUNT'=>'Subtotal',
'LBL_SUBTOTAL_TAX_AMOUNT'=>'Subtotal + Impostos',
'LBL_TOTAL_AMT'=>'Total',
'LBL_GROUP_TOTAL'=>'Total do Grupo',
);
?>
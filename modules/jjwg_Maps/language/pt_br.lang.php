<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_CONFIG_ADDRESS_CACHE_GET_ENABLED_DESC'=>'&#039;address_cache_get_enabled&#039; permite que o módulo cache de endereço para recuperar os dados da tabela de cache.',
'LBL_CONFIG_ADDRESS_CACHE_SAVE_ENABLED_DESC'=>'&#039;address_cache_save_enabled&#039; permite que o módulo cache de endereço para salvar os dados para a tabela de cache.',
'LBL_CONFIG_ALLOW_APPROXIMATE_LOCATION_TYPE_DESC'=>'&#039;allow_approximate_location_type&#039; - permite que tipos de locais de &#039;APPROXIMATE&#039; a ser considerado resultados de geocodificação &#039;OK&#039;.',
'LBL_CONFIG_EXPORT_ADDRESSES_LIMIT_DESC'=>'&#039;export_addresses_limit&#039; define o limite de consulta ao selecionar os registros de exportação.',
'LBL_CONFIG_GEOCODING_LIMIT_DESC'=>'&#039;geocoding_limit&#039; define o limite de consulta ao selecionar registros para geocode.',
'LBL_CONFIG_GOOGLE_GEOCODING_LIMIT_DESC'=>'&#039;google_geocoding_limit&#039; define o limite do pedido quando a geocodificação utilizando a API do Google Maps.',
'LBL_CONFIG_LOGIC_HOOKS_ENABLED_DESC'=>'&#039;logic_hooks_enabled&#039; permite ganchos de lógica para a atualização automática com base em objetos relacionados Recomenda-se desativar quando atualizar seu SugarCRM.',
'LBL_CONFIG_MAP_CLUSTER_GRID_SIZE_DESC'=>'&#039;map_clusterer_grid_size&#039; é usado para definir o tamanho da grade de cálculo mapa clusterers.',
'LBL_CONFIG_MAP_MARKERS_CLUSTERER_MAX_ZOOM_DESC'=>'&#039;map_clusterer_max_zoom&#039; é usado para definir o nível máximo de zoom em que não será aplicada clustering.',
'LBL_CONFIG_MAP_DEFAULT_CENTER_LATITUDE_DESC'=>'&#039;map_default_center_latitude&#039; define a posição latitude centro padrão para mapas.',
'LBL_CONFIG_MAP_DEFAULT_CENTER_LONGITUDE_DESC'=>'&#039;map_default_center_longitude&#039; define a posição longitude centro padrão para mapas.',
'LBL_CONFIG_MAP_DEFAULT_DISTANCE_DESC'=>'&#039;map_default_distance&#039; define a distância padrão usado para os mapas baseados em distância.',
'LBL_CONFIG_MAP_DEFAULT_UNIT_TYPE_DESC'=>'&#039;map_default_unit_type&#039; define o tipo de unidade de medida padrão para cálculos de distância Valores: &#039;Mi&#039; ( milhas) ou «km» (km)',
'LBL_CONFIG_MAP_DUPLICATE_MARKER_ADJUSTMENT_DESC'=>'&#039;map_duplicate_marker_adjustment&#039; define um ajustamento de compensação a ser adicionada a longitude e latitude no caso da posição do marcador em duplicado.',
'LBL_CONFIG_MAP_MARKERS_LIMIT_DESC'=>'&#039;map_markers_limit&#039; define o limite de consulta ao selecionar registros a serem exibidos em um mapa.',
'LBL_CONFIG_OF_RELATED_ACCOUNT_OPPORTUNITY'=>'( de Conta / Oportunidade relacionados )',
'LBL_CONFIG_OF_RELATED_ACCOUNT'=>'( de Conta relacionadas )',
'LBL_CONFIG_GEOCODING_API_URL_DESC'=>'A URL do Google Maps API V3 ou Proxy',
'LBL_CONFIG_GEOCODING_API_SECRET_DESC'=>'A frase secreta para ser usado com a comparação MD5 Proxy.',
'LBL_GEOCODED_COUNTS_DESCRIPTION'=>'A tabela mostra belown mostra o número de objetos do módulo geocodificados, agrupados por geocoding resposta. Tenha em mente que o padrão Google Maps limite de utilização é de 2500 pedidos por dia. Este módulo irá armazenar em cache os endereços de geocodificação de informações durante o processamento para reduzir o número total de pedidos necessários.',
'LNK_NEW_RECORD'=>'Adicionar Novo Mapa',
'LNK_NEW_MAP'=>'Adicionar Novo Mapa',
'LBL_ADD_TO_TARGET_LIST'=>'Adicionar à Lista de Destino',
'LBL_ENABLED'=>'Ativado',
'LBL_CONFIG_ADDRESS_CACHE_GET_ENABLED_TITLE'=>'Ativar Cache Endereço (Get ):',
'LBL_CONFIG_ADDRESS_CACHE_SAVE_ENABLED_TITLE'=>'Ativar Salvando Endereço Cache ( Save):',
'LBL_CONFIG_LOGIC_HOOKS_ENABLED_TITLE'=>'Ativar Todos os Logic Hooks:',
'LBL_MAP_ASSIGNED_TO'=>'Atribuído a:',
'LBL_BUG_FIX'=>'Bug Fix',
'LBL_CRON_URL'=>'CRON URL',
'LBL_ADDRESS_CACHE'=>'Cache de Endereço',
'LBL_CONFIG_GROUP_FIELD_FOR'=>'Campo Grupo para',
'LBL_CONFIG_GROUP_FIELD_FOR_ACCOUNTS'=>'Campo Grupo para Contas:',
'LBL_CONFIG_GROUP_FIELD_FOR_CONTACTS'=>'Campo Grupo para Contatos:',
'LBL_CONFIG_GROUP_FIELD_FOR_LEADS'=>'Campo Grupo para Leads:',
'LBL_CONFIG_GROUP_FIELD_FOR_CASES'=>'Campo Grupo para Ocorrências:',
'LBL_CONFIG_GROUP_FIELD_FOR_OPPORTUNITIES'=>'Campo Grupo para Oportunidades:',
'LBL_CONFIG_GROUP_FIELD_FOR_PROSPECTS'=>'Campo Grupo para Perspectivas / Metas:',
'LBL_CONFIG_GROUP_FIELD_FOR_PROJECTS'=>'Campo Grupo para Projetos:',
'LBL_CONFIG_GROUP_FIELD_FOR_MEETINGS'=>'Campo Grupo para Reuniões:',
'LBL_CONFIG_MAP_ADSENSE_REMOVAL_KEY_TITLE'=>'Chave Remoção Mapa AdSense:',
'LBL_CONFIG_LOGIC_HOOKS_SETTINGS_TITLE'=>'Configuração dos Logic Hooks:',
'LBL_CONFIG_TITLE'=>'Configurações',
'LBL_CONFIG_ADDRESS_CACHE_SETTINGS_TITLE'=>'Configurações de Cache de Endereço:',
'LBL_CONFIG_ADDRESS_TYPE_SETTINGS_TITLE'=>'Configurações de tipo de endereço: Este define os« tipos de endereços usados ​​quando os endereços de geocodificação dos valores aceitáveis. : os módulos de faturamento, transporte&#039;; &#039;primárias&#039;; &#039;alt&#039;; &#039;flex_relate&#039;',
'LBL_CONFIG_SAVED'=>'Configurações salvas com sucesso!',
'LBL_MAP_GEOCODED_COUNTS'=>'Contador de Geocode',
'LBL_GEOCODED_COUNTS'=>'Contador do Módulo Geocode',
'LBL_ADDRESS_CUSTOM'=>'Custom ( Controlador Lógico Custom)',
'LBL_DISABLED'=>'Desativado',
'LBL_DISTANCE'=>'Distância (Raio):',
'LBL_MAP_DONATE'=>'Doar',
'LBL_MAP_DONATE_TO_THIS_PROJECT'=>'Doar ao Projeto',
'LBL_CONFIG_MAP_ADSENSE_REMOVAL_KEY_DESC'=>'Doações para este projecto! Entre em contato JJWDesign.com para uma chave de remoção AdSense.',
'LBL_MAP_ADDRESS'=>'Endereço',
'LBL_ADDRESS_ADDRESS'=>'Endereço ( Simples, Usuários )',
'LBL_ALTERNATIVE_ADDRESS'=>'Endereço Alternativo',
'LBL_MAP_GEOCODE_ADDRESSES'=>'Endereço Geocode',
'LBL_PRIMARY_ADDRESS'=>'Endereço Principal',
'LBL_SHIPPING_ADDRESS'=>'Endereço de Entrega',
'LBL_BILLING_ADDRESS'=>'Endereço de Faturamento',
'LBL_CONFIG_EXPORT_ADDRESSES_LIMIT_TITLE'=>'Exportação Limite Endereços:',
'LBL_EXPORT_ADDRESS_URL'=>'Exportação URLs',
'LBL_ADDRESS_FLEX_RELATE'=>'Flex Relacionar',
'LBL_CONFIG_GEOCODING_API_SECRET_TITLE'=>'Frase Secreta para Proxy:',
'LBL_MAP_ADDRESS_TEST'=>'Geocodificação Teste',
'LBL_CONFIG_GEOCODING_SETTINGS_TITLE'=>'Geocoding / Google Settings:',
'LBL_MAP_GROUP'=>'Grupo',
'LBL_MAP_USER_GROUPS'=>'Grupos de Usuários:',
'LNK_IMPORT_MAPS'=>'Importação',
'LBL_MAP_LEGEND'=>'Legenda:',
'LBL_CONFIG_GOOGLE_GEOCODING_LIMIT_TITLE'=>'Limite de geocodificação Google:',
'LBL_CONFIG_GEOCODING_LIMIT_TITLE'=>'Limite de geocodificação:',
'LNK_MAP_LIST'=>'Lista de Mapas',
'LBL_HOMEPAGE_TITLE'=>'Listagem Mapas',
'LBL_LIST_FORM_TITLE'=>'Listagem Mapas',
'LBL_CONFIG_MAP_MARKERS_CLUSTERER_MAX_ZOOM_TITLE'=>'Map Markers Clusterer Max Zoom:',
'LBL_CONFIG_MAP_MARKERS_LIMIT_TITLE'=>'Map Markers Limite:',
'LBL_MAP'=>'Mapa',
'LBL_CONFIG_MAP_DUPLICATE_MARKER_ADJUSTMENT_TITLE'=>'Mapa Duplicate Ajuste Marcador:',
'LBL_CONFIG_MAP_DEFAULT_CENTER_LATITUDE_TITLE'=>'Mapa Padrão Centro Latitude:',
'LBL_CONFIG_MAP_DEFAULT_CENTER_LONGITUDE_TITLE'=>'Mapa Padrão Longitude Center:',
'LBL_CONFIG_MAP_DEFAULT_UNIT_TYPE_TITLE'=>'Mapa Padrão Tipo de unidade:',
'LBL_MAP_QUICK_RADIUS'=>'Mapa Raio Rápido',
'LBL_MODULE_ID'=>'Mapas',
'LBL_MODULE_NAME'=>'Mapas',
'LBL_MAPS'=>'Mapas',
'LBL_MODULE_TITLE'=>'Mapas: Home',
'LBL_MAP_ACTION'=>'Mapear',
'LBL_MAP_CUSTOM_MARKER'=>'Marcador',
'LBL_CONFIG_VALID_GEOCODE_MODULES'=>'Módules Geocode válidos:',
'LBL_MODULE_HEADING'=>'Módulo',
'LBL_MODULE_TYPE'=>'Módulo de Visualização:',
'LBL_MAP_NULL_GROUP_NAME'=>'Nada',
'LBL_CONFIG_CUSTOM_CONTROLLER_DESC'=>'Nota Importante: Todas as configurações salvas podem ser encontrados na tabela &#039;config&#039; na categoria &#039;jjwg&#039; Note, um arquivo controller.php costume não deve ser usado para substituir as configurações.',
'LBL_CONFIG_RELATED_OBJECT_THRU_FLEX_RELATE'=>'Objeto relacionado através do Campo Flex Relate',
'LBL_MAP_GET_DIRECTIONS'=>'Obter Direções',
'LBL_CONFIG_DEFAULT'=>'Padrão:',
'LBL_DEFAULT'=>'Padrão:',
'LBL_CRON_INSTRUCTIONS'=>'Para processar os pedidos de geocodificação, é recomendado configurar uma noite Cron Job. Um ponto de acesso personalizado tenha sido criado para esse fim e pode ser acessada sem autenticação. O URL mostrado abaixo é para ser usado com um Scheduled Task Administrativo. Por favor, consulte a documentação do SugarCRM para obter mais informações.',
'LBL_CONFIG_ALLOW_APPROXIMATE_LOCATION_TYPE_TITLE'=>'Permitir &#039;APPROXIMATE&#039; as «Localização Tipos:',
'LBL_ADD_TO_TARGET_LIST_PROCESSING'=>'Processando...',
'LBL_MAP_PROCESS'=>'Processar',
'LBL_FLEX_RELATE'=>'Relacionado ao (Centro):',
'LBL_MODULE_RESET_HEADING'=>'Reset',
'LBL_CONFIG_VALID_GEOCODE_TABLES'=>'Tabelas Geocode válidas:',
'LBL_CONFIG_MAP_CLUSTER_GRID_SIZE_TITLE'=>'Tamanho da Grade do Map Markers Clusterer:',
'LBL_ADD_TO_TARGET_LIST_CONFIRM'=>'Tem certeza que você deseja adicionar os itens selecionados para a lista de alvos?',
'LBL_MAP_TYPE'=>'Tipo',
'LBL_CONFIG_ADDRESS_TYPE_FOR_CONTACTS'=>'Tipo de Endereço para Contatos:',
'LBL_CONFIG_ADDRESS_TYPE_FOR_LEADS'=>'Tipo de Endereço para Leads:',
'LBL_CONFIG_ADDRESS_TYPE_FOR_CASES'=>'Tipo de Endereço para Ocorrências:',
'LBL_CONFIG_ADDRESS_TYPE_FOR_OPPORTUNITIES'=>'Tipo de Endereço para Oportunidades:',
'LBL_CONFIG_ADDRESS_TYPE_FOR_PROJECTS'=>'Tipo de Endereço para Projetos:',
'LBL_CONFIG_ADDRESS_TYPE_FOR_MEETINGS'=>'Tipo de Endereço para Reuniões:',
'LBL_CONFIG_ADDRESS_TYPE_FOR_ACCOUNTS'=>'Tipo de Endereço para as Contas:',
'LBL_UNIT_TYPE'=>'Tipo de Unidade:',
'LBL_CONFIG_ADDRESS_TYPE_FOR'=>'Tipo de endereço para',
'LBL_CONFIG_ADDRESS_TYPE_FOR_PROSPECTS'=>'Tipo de endereço para Prospects / Metas:',
'LBL_MODULE_TOTAL_HEADING'=>'Total',
'LBL_CONFIG_GEOCODING_API_URL_TITLE'=>'URL Geocoding API:',
'LBL_EXPORT_INSTRUCTIONS'=>'Use os links abaixo para exportar endereços completos na necessidade de geocodeing informação. Em seguida, use um lote online ou offline geocoding ferramenta para o georreferenciamento dos endereços. Quando você terminar de geocodificação, importar os endereços para o módulo de cache de endereços para ser usado com os seus mapas. Note, o endereço do módulo de cache é opcional. Todas as informações geocodificação é armazenado no módulo representativo.',
'LBL_MAP_USERS'=>'Usuários:',
'LBL_MAP_DISPLAY'=>'Visualização do Mapa',
'LBL_CONFIG_MAP_DEFAULT_DISTANCE_TITLE'=>'mapa padrão a distância',
'LBL_CONFIG_MARKER_MAPPING_SETTINGS_TITLE'=>'marcador / Mapping configurações:',
'LBL_CONFIG_MARKER_GROUP_FIELD_SETTINGS_TITLE'=>'marcador Grupo Configurações de campo: isto define o &#039;campo&#039; para ser usado como parâmetro grupo ao exibir marcadores em um mapa Exemplos: . assigned_user_name , indústria, status, sales_stage , prioritárias',
'LBL_MAP_CUSTOM_AREA'=>'Área',
'LBL_MAP_LAST_STATUS'=>'Último status Geocode',
);
?>
<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_ACTIVE'=>'Ativa',
'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'Atividades',
'LBL_ASSIGNED_TO_NAME'=>'Atribuido a ',
'LBL_WARNING_OVERWRITE'=>'Aviso: Isto vai sobrepor seu trabalho atual',
'LBL_HEADER'=>'Cabeçalho',
'LBL_SAMPLE'=>'Carga Exemplo',
'LBL_ANY_TOWN'=>'Cidade',
'LBL_DESCRIPTION'=>'Corpo',
'LBL_CREATED_ID'=>'Criado Por',
'LBL_CREATED_USER'=>'Criado Por',
'LBL_CREATED'=>'Criado Por',
'LNK_NEW_RECORD'=>'Criar Formatos PDF',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DELETED'=>'Deletado',
'LBL_ANY_STREET'=>'Endereço',
'LBL_ACCOUNT_SAMPLE'=>'Exemplo de Conta',
'LBL_CONTACT_SAMPLE'=>'Exemplo de Contato',
'LBL_QUOTE_SAMPLE'=>'Exemplo de Cotação',
'LBL_QUOTE_GROUP_SAMPLE'=>'Exemplo de Grupo de Cotação',
'LBL_INVOICE_GROUP_SAMPLE'=>'Exemplo de Grupo de Fatura',
'LBL_LEAD_SAMPLE'=>'Exemplo de Lead',
'LBL_INVOICE_SAMPLE'=>'Exemplo de Pedido',
'LBL_DEFAULT_TEMPLATE'=>'Formato Default',
'LBL_AOS_PDF_TEMPLATES_SUBPANEL_TITLE'=>'Formatos PDF',
'LBL_MODULE_NAME'=>'Formatos PDF',
'LBL_MODULE_TITLE'=>'Formatos PDF',
'LBL_ID'=>'ID',
'LBL_ASSIGNED_TO_ID'=>'Id do Usuário Atribuido',
'LBL_BUTTON_INSERT'=>'Inserir',
'LBL_INSERT_FIELDS'=>'Inserir Campos',
'LBL_LIST_FORM_TITLE'=>'Lista de Formatos PDF',
'LBL_ANY_WHERE'=>'Lugar',
'LBL_MARGIN_RIGHT'=>'Margem Direita',
'LBL_MARGIN_LEFT'=>'Margem Esquerda',
'LBL_MARGIN_BOTTOM'=>'Margem Inferior',
'LBL_MARGIN_HEADER'=>'Margem do Cabeçalho',
'LBL_MARGIN_FOOTER'=>'Margem do Rodapé',
'LBL_MARGIN_TOP'=>'Margem do Topo',
'LBL_EDITVIEW_PANEL1'=>'Margens',
'LBL_DETAILVIEW_PANEL1'=>'Margens',
'LBL_HOMEPAGE_TITLE'=>'Meus Formatos PDF',
'LBL_MODIFIED_ID'=>'Modificado pelo Id',
'LBL_MODIFIED_NAME'=>'Modificado pelo Nome',
'LBL_MODIFIED_USER'=>'Modificado pelo Usuário',
'LBL_MODIFIED'=>'Modificado por',
'LBL_NAME'=>'Nome',
'LBL_NEW_FORM_TITLE'=>'Novos Formatos PDF',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar Formatos PDF',
'LBL_PREPARED_FOR'=>'Preparado Para',
'LBL_PREPARED_BY'=>'Preparado Por',
'LBL_PAGE'=>'Página',
'LBL_FOOTER'=>'Rodapé',
'LBL_TYPE'=>'Tipo',
'LNK_LIST'=>'Visualizar Formatos PDF',
'LBL_HISTORY_SUBPANEL_TITLE'=>'Visualizar Histórico',
);
?>
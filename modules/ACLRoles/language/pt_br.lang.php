<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_CREATE_ROLE'=>'Criar Perfil',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_DUPLICATE_OF'=>'Duplicar De',
'LBL_EDIT_VIEW_DIRECTIONS'=>'Duplo clique para alterar o valor na célula',
'LIST_ROLES'=>'Listar Perfis',
'LIST_ROLES_BY_USER'=>'Listar Perfis por Usuário',
'LBL_NAME'=>'Nome',
'LBL_ACCESS_DEFAULT'=>'Padrão',
'LBL_LIST_FORM_TITLE'=>'Perfis',
'LBL_ROLE'=>'Perfis',
'LBL_MODULE_NAME'=>'Perfis',
'LBL_ROLES_SUBPANEL_TITLE'=>'Perfis do Usuário',
'LBL_MODULE_TITLE'=>'Perfis: Principal',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar',
'LBL_ACTION_ADMIN'=>'Tipo de Acesso',
'LBL_ALL'=>'Tudo',
'LBL_USERS_SUBPANEL_TITLE'=>'Usuários',
);
?>
<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_CREATED_BY'=>'Alterado Por',
'LBL_FIELD_NAME'=>'Campo',
'LBL_AUDITED_FIELDS'=>'Campos auditados neste módulo: ',
'LBL_LIST_DATE'=>'Data da Modificação',
'LBL_NEW_VALUE'=>'Novo Valor',
'LBL_NO_AUDITED_FIELDS_TEXT'=>'Não existem campos auditados neste módulo',
'LBL_CHANGE_LOG'=>'Registro de Alterações',
'LBL_OLD_NAME'=>'Valor Anterior',
);
?>
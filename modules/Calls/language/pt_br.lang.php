<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_HOURS_MINUTES'=>'(horas/minutos)',
'LBL_BLANK'=>'-Nenhuma-',
'LBL_COLON'=>':',
'LBL_ACCEPT_LINK'=>'Aceitar Link',
'LBL_ACCEPT_STATUS'=>'Aceitar Status',
'LBL_ADD_BUTTON'=>'Adicionar',
'LBL_ADD_INVITEE'=>'Adicionar convidados',
'LBL_SCHEDULING_FORM_TITLE'=>'Agenda',
'LBL_HISTORY_SUBPANEL_TITLE'=>'Anotações ou Anexos',
'LBL_LIST_MY_CALLS'=>'As Minhas Ligações',
'LBL_EXPORT_ASSIGNED_USER_ID'=>'Assigned User ID',
'LBL_LIST_SUBJECT'=>'Assunto',
'LBL_SUBJECT'=>'Assunto:',
'LBL_LIST_ASSIGNED_TO_NAME'=>'Atribuído a',
'LBL_ASSIGNED_TO_NAME'=>'Atribuído a',
'LBL_ASSIGNED_TO_ID'=>'Atribuído a:',
'LBL_REMINDER'=>'Aviso:',
'LBL_CANCEL_CREATE_INVITEE'=>'Cancelar',
'LBL_CREATE_CONTACT'=>'Como Contato',
'LBL_CREATE_LEAD'=>'Como Potencial',
'LBL_REPEAT_COUNT'=>'Contador de Repetições',
'LBL_LIST_CONTACT'=>'Contato',
'LBL_CONTACT_NAME'=>'Contato:',
'LBL_CONTACTS_SUBPANEL_TITLE'=>'Contatos',
'LBL_INVITEE'=>'Convidados',
'LBL_EXPORT_CREATED_BY'=>'Created By ID',
'LBL_CREATED_ID'=>'Criado Por',
'LBL_CREATED_USER'=>'Criado Por',
'LBL_CREATED'=>'Criado Por',
'LBL_NEW_FORM_TITLE'=>'Criar Compromisso',
'LBL_CREATE_INVITEE'=>'Criar Convidado',
'LBL_CREATE_AND_ADD'=>'Criar e Adicionar',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_LIST_DATE'=>'Data de Início',
'LBL_DATE'=>'Data de Início:',
'LBL_DATE_END'=>'Data de conclusão',
'LBL_DATE_TIME'=>'Data e Hora de Início:',
'LBL_DEL'=>'Del',
'LBL_DELETED'=>'Deletado',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_EMPTY_SEARCH_RESULT'=>'Desculpe, nenhum registro foi encontrado. Por Favor, crie um convidado abaixo.',
'LBL_LIST_DIRECTION'=>'Direção',
'LBL_DIRECTION'=>'Direção:',
'LBL_LIST_DURATION'=>'Duração',
'LBL_DURATION_HOURS'=>'Duração (horas):',
'LBL_DURATION_MINUTES'=>'Duração (minutos):',
'LBL_DURATION'=>'Duração:',
'LBL_EDIT_BUTTON'=>'Editar',
'LBL_EMAIL'=>'Email',
'LBL_REMINDER_EMAIL'=>'Email',
'LBL_REMINDER_EMAIL_ALL_INVITEES'=>'Enviar Email a todos os convidados',
'LBL_SEND_BUTTON_LABEL'=>'Enviar convites',
'LBL_SEND_BUTTON_TITLE'=>'Enviar convites [Alt+I]',
'LBL_LIST_CLOSE'=>'Fechar',
'LBL_LIST_TIME'=>'Hora de Início',
'LBL_TIME'=>'Hora de Início:',
'LBL_TIME_END'=>'Hora de conclusão',
'LBL_REMINDER_TIME'=>'Hora do aviso',
'LBL_SEND_BUTTON_KEY'=>'I',
'LBL_ID'=>'ID',
'LNK_IMPORT_CALLS'=>'Importar Ligações',
'LBL_CALL_INFORMATION'=>'Informação da Ligação',
'LBL_DESCRIPTION_INFORMATION'=>'Informações da Descrição',
'LBL_REPEAT_INTERVAL'=>'Intervalo da Repetição',
'LBL_MODULE_NAME'=>'Ligações',
'LBL_DEFAULT_SUBPANEL_TITLE'=>'Ligações',
'LBL_CALL'=>'Ligações:',
'LBL_MODULE_TITLE'=>'Ligações: Tela Principal',
'LBL_LIST_FORM_TITLE'=>'Listar Ligações',
'LBL_MEMBER_OF'=>'Membro de',
'LBL_MODIFIED_NAME'=>'Modificado Por',
'LBL_MODIFIED_ID'=>'Modificado Por',
'LBL_MODIFIED_USER'=>'Modificado Por',
'LBL_MODIFIED'=>'Modificado Por',
'LBL_EXPORT_MODIFIED_USER_ID'=>'Modified By ID',
'LBL_FIRST_NAME'=>'Nome',
'LBL_LIST_NAME'=>'Nome',
'LBL_NAME'=>'Nome Completo',
'LNK_NEW_ACCOUNT'=>'Nova Conta',
'LNK_NEW_CALL'=>'Nova Ligação',
'LNK_NEW_OPPORTUNITY'=>'Nova Oportunidade',
'LNK_NEW_MEETING'=>'Nova Reunião',
'LNK_NEW_APPOINTMENT'=>'Novo Compromisso',
'LBL_OUTLOOK_ID'=>'Outlook ID',
'LBL_PARENT_ID'=>'Parent ID',
'LBL_SEARCH_BUTTON'=>'Pesquisar',
'LBL_SEARCH_FORM_TITLE'=>'Pesquisar Ligações',
'LBL_DEFAULT_STATUS'=>'Planejado',
'LBL_REMINDER_POPUP'=>'Popup',
'LBL_SELECT_FROM_DROPDOWN'=>'Por favor faça primeiro uma selecção da lista Referente A',
'LBL_LEADS_SUBPANEL_TITLE'=>'Potenciais',
'LBL_RECURRING_SOURCE'=>'Recurring Source',
'LBL_LIST_RELATED_TO'=>'Referente a',
'LBL_RELATED_TO'=>'Referente a:',
'LBL_LIST_RELATED_TO_ID'=>'Referente ao ID',
'LBL_LOG_CALL'=>'Registo de Ligações',
'LBL_EXPORT_PARENT_TYPE'=>'Related To Module',
'LBL_ACTIVITIES_REPORTS'=>'Relatório de Atividades',
'LBL_EXPORT_REMINDER_TIME'=>'Reminder Time (in minutes)',
'LBL_REPEAT_DOW'=>'Repeat Dow',
'LBL_REPEAT_PARENT_ID'=>'Repetir ID Pai',
'LBL_REPEAT_UNTIL'=>'Repetir até',
'LNK_SELECT_ACCOUNT'=>'Selecionar Conta',
'LBL_EXPORT_DATE_START'=>'Start Date and Time',
'LBL_STATUS'=>'Status:',
'LBL_PHONE'=>'Telefone',
'NTC_REMOVE_INVITEE'=>'Tem certeza que deseja remover este convidado da Ligação?',
'NOTICE_DURATION_TIME'=>'Tempo de duração deve ser maior que 0',
'LBL_SYNCED_RECURRING_MSG'=>'This call originated in another system and was synced to Sugar. To make changes, go to the original call within the other system. Changes made in the other system can be synced to this record.',
'LBL_REPEAT_TYPE'=>'Tipo de Repetição',
'ERR_DELETE_RECORD'=>'Um número de registo deve ser especificado para excluir a conta',
'LBL_USERS_SUBPANEL_TITLE'=>'Usuários',
'LNK_CALL_LIST'=>'Visualizar Ligações',
'LBL_NO_ACCESS'=>'Você não tem acesso para criar $module',
'LBL_HOURS_ABBREV'=>'h',
'LBL_MINSS_ABBREV'=>'m',
'LBL_REMOVE'=>'rem',
'LBL_DATE_MODIFIED'=>'Última Modificação',
'LBL_LAST_NAME'=>'Último Nome',
);
?>
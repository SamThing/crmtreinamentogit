<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $app_list_strings = array ( 
 '_subcategory_dom' => array ( 
''=>'',
''=>'-Nenhuma-',
'Marketing Collateral'=>'Acessório de Marketing',
'Product Brochures'=>'Brochuras de Produtos',
'FAQ'=>'FAQ',
),
 '_status_dom' => array ( 
''=>'',
'Active'=>'Activo',
'Under Review'=>'Em Revisão',
'Expired'=>'Expirou',
'FAQ'=>'FAQ',
'Pending'=>'Pendente',
'Draft'=>'Rascunho',
),
 '_category_dom' => array ( 
''=>'',
''=>'-Nenhuma-',
'Knowledege Base'=>'Base de Conhecimento',
'Marketing'=>'Marketing',
'Sales'=>'Vendas',
),
);
?>
<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_SUBJECT'=>'Assunto',
'LBL_ASSIGNED_TO_ID'=>'Atribuído a',
'LBL_ASSIGNED_TO_NAME'=>'Atribuído a',
'LBL_CREATED_BY'=>'Criado por',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_CREATED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_EDIT_BUTTON'=>'Editar',
'LBL_SYSTEM_ID'=>'Id de Sistema',
'LBL_NAME'=>'Nome',
'LBL_TEAM_NAME'=>'Nome da Equipa',
'LBL_NUMBER'=>'Número',
'LBL_PRIORITY'=>'Prioridade',
'LBL_WORK_LOG'=>'Registo de Trabalho',
'LBL_REMOVE'=>'Remover',
'LBL_RESOLUTION'=>'Resolução',
'LBL_STATUS'=>'Status',
'LBL_TYPE'=>'Tipo',
'LBL_ASSIGNED_USER'=>'Usuário Atribuído',
'LBL_LAST_MODIFIED'=>'Última Modificação',
'LBL_MODIFIED_BY'=>'Última Modificação por',
);
?>
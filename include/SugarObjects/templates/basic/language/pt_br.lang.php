<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_CREATED'=>'Criado Por',
'LBL_CREATED_ID'=>'Criado Por Id',
'LBL_CREATED_USER'=>'Criado por',
'LBL_DATE_ENTERED'=>'Data da Criação',
'LBL_DATE_MODIFIED'=>'Data da Modificação',
'LBL_DELETED'=>'Deletado',
'LBL_DESCRIPTION'=>'Descrição',
'LBL_EDIT_BUTTON'=>'Editar',
'LBL_ID'=>'ID',
'LBL_MODIFIED_NAME'=>'Modificado Por',
'LBL_MODIFIED'=>'Modificado Por',
'LBL_MODIFIED_ID'=>'Modificado Por Id',
'LBL_MODIFIED_USER'=>'Modificado por',
'LBL_LIST_NAME'=>'Nome',
'LBL_NAME'=>'Nome',
'LBL_REMOVE'=>'Remover',
);
?>
<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_INTERNAL'=>'Atualização Interna',
'LBL_AOP_CASE_UPDATES'=>'Atualização de Ocorrência',
'LBL_CASE_UPDATES_COLLAPSE_ALL'=>'Comprimir Todos',
'LBL_AOP_CASE_EVENTS'=>'Eventos de Ocorrência',
'LBL_CASE_UPDATES_EXPAND_ALL'=>'Expandir Todos',
'LBL_SECURITYGROUPS_SUBPANEL_TITLE'=>'Grupos de Segurança',
'LBL_AOP_CASE_UPDATES_THREADED'=>'Listagem de Atualização de Ocorrência',
'LBL_NO_CASE_UPDATES'=>'Não há atualizações para esta ocorrência',
'LBL_UPDATE_TEXT'=>'Texto de Atualização',
);
?>
<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_CONTRIBUTOR_SUITECRM'=>'A alternativa gratuita e open source a Edição Profissional do SugarCRM.',
'LBL_CONTRIBUTORS'=>'Contribuidores',
'LBL_CONTRIBUTOR_JJW_GMAPS'=>'JJWDesign Google Maps por Jeffrey J. Walters',
'LBL_ABOUT_SUITE_5'=>'O suporte ao SuiteCRM está disponível na opções livre e paga.',
'LBL_FEATURING'=>'Os Módulos de AOS, AOW, AOR, AOP, AOE e Replanejamento são da SalesAgility.',
'LBL_PARTNERS'=>'Parceiros',
'LBL_CONTRIBUTOR_QUICKCRM'=>'QuickCRM Mobile por Benoit Luquet',
'LBL_CONTRIBUTOR_SECURITY_SUITE'=>'SecuritySuite por Jason Eggers',
'LBL_ABOUT'=>'Sobre',
'LBL_ABOUT_SUITE'=>'Sobre o SuiteCRM',
'LBL_CONTRIBUTOR_CONSCIOUS'=>'SuiteCRM LOGO fornecido por Conscious Solutions',
'LBL_ABOUT_SUITE_2'=>'SuiteCRM é publicado sobre licença open source - GPL3',
'LBL_ABOUT_SUITE_3'=>'SuiteCRM é totalmente compatível com o Sugar CRM 6.5.x',
'LBL_ABOUT_SUITE_1'=>'SuiteCRM é uma derivação do SugarCRM. Há vários artigos na internet que explicam as razões pelas quais esta derivação tornou-se necessária.',
'LBL_SUITE_PARTNERS'=>'Temos parceiros SuiteCRM leais e apaixonados pelo open source. Para ver a nossa lista completa de parceiros consulte o nosso site.',
'LBL_ABOUT_SUITE_4'=>'Todo o código administrado e desenvolvido pelo projeto será liberado como open source - GPL3',
'LBL_LANGUAGE_SPANISH'=>'Tradução para o Espanhol fornecida por Disytel openConsultin',
'LBL_LANGUAGE_RUSSIAN'=>'Tradução para o Russo por Likhobory',
);
?>
<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_CBR_TRANSLATION_ACTIVITIES_CALLS_FROM_CBR_TRANSLATION_TITLE'=>'Atividades:Tradução',
'LBL_RESCHEDULE_DATE'=>'Data',
'LBL_RESCHEDULE_ERROR2'=>'Favor seleciona um motivo',
'LBL_RESCHEDULE_ERROR1'=>'Favor selecionar uma data válida',
'LBL_SECURITYGROUPS_SUBPANEL_TITLE'=>'Grupos de Segurança',
'LBL_RESCHEDULE_HISTORY'=>'Histórico de Tentativa de Ligações',
'LBL_RESCHEDULE_REASON'=>'Motivo',
'LBL_RESCHEDULE_PANEL'=>'Reagendamento',
'LBL_RESCHEDULE'=>'Reagendamento',
'LBL_RESCHEDULE_COUNT'=>'Tentativa de Ligações',
);
?>
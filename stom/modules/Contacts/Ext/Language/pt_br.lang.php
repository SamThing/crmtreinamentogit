<?php 
 if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
 $mod_strings = array ( 
'LBL_ENABLE_PORTAL_USER'=>'Ativar Usuário do Portal',
'LBL_ENABLE_PORTAL_USER_SUCCESS'=>'Ativar Usuário do Portal',
'LBL_AOP_CASE_UPDATES'=>'Atualização de Ocorrências',
'LBL_CREATE_PORTAL_USER_SUCCESS'=>'Criar Usuário para Portal',
'LBL_CREATE_PORTAL_USER'=>'Criar usuário para o Portal',
'LBL_FP_EVENT_DELEGATES_CONTACTS_1_FROM_FP_EVENT_DELEGATES_TITLE'=>'Delegados',
'LBL_DISABLE_PORTAL_USER_SUCCESS'=>'Desativar Usuário do Portal',
'LBL_DISABLE_PORTAL_USER'=>'Desativar Usuário do Portal',
'LBL_FP_EVENTS_CONTACTS_FROM_FP_EVENTS_TITLE'=>'Eventos',
'LBL_ENABLE_PORTAL_USER_FAILED'=>'Falha ao ativar o usuário do Portal',
'LBL_DISABLE_PORTAL_USER_FAILED'=>'Falha ao desativar usuário do Portal',
'LBL_CREATE_PORTAL_USER_FAILED'=>'Falha na Criação do Usuário',
'LBL_SECURITYGROUPS_SUBPANEL_TITLE'=>'Grupos de Segurança',
'LBL_NO_JOOMLA_URL'=>'Nenhum URL foi especificada para o Portal',
'LBL_PORTAL_USER_TYPE'=>'Tipo do Usuário do Portal',
);
?>